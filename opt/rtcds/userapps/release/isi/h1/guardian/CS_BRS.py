from guardian import GuardState
import time
#######################
request = 'INIT'
nominal = 'RUN'

class INIT(GuardState):
    index = 0
    def main(self):
        return 'RUN'

class RUN(GuardState):
    index = 30
    def main(self):
        return True
    def run(self):
        if ezca['NGN-CS_CBRS_RY_PZT1_MASTER_OUTMON'] < -70000:
            return 'TURN_OFF_DAMPING'
        if ezca['NGN-CS_CBRS_RY_PZT1_MASTER_OUTMON'] > 130000:
            return 'TURN_OFF_DAMPING'
        if ezca['NGN-CS_CBRS_RY_PZT2_MASTER_OUTMON'] > 130000:
            return 'TURN_OFF_DAMPING'
        if ezca['NGN-CS_CBRS_RY_PZT2_MASTER_OUTMON'] < -70000:
            return 'TURN_OFF_DAMPING'
        else:
            return True

class TURN_OFF_DAMPING(GuardState):
    index = 5
    def main(self):
	    ezca['NGN-CS_CBRS_RY_DAMP_GAIN'] = 0
	    self.timer['damp_off'] = ezca['NGN-CS_CBRS_RY_DAMP_TRAMP']

    def run(self):
        if self.timer['damp_off']:
            return 'TURN_OFF_PZT_CTRL'


class TURN_OFF_PZT_CTRL(GuardState):
    index = 10
    def main(self):
	    ezca['NGN-CS_CBRS_RY_PZT2_CTRL_GAIN'] = 0
	    ezca['NGN-CS_CBRS_RY_PZT1_CTRL_GAIN'] = 0
	    self.timer['ctrl_off'] = ezca['NGN-CS_CBRS_RY_PZT1_CTRL_TRAMP']+2

    def run(self):
        if self.timer['ctrl_off']:
            return 'CLEAR_HISTORY'


class CLEAR_HISTORY(GuardState):
    index = 15
    def main(self):
	    ezca['NGN-CS_CBRS_RY_PZT2_CTRL_RSET'] = 2
	    ezca['NGN-CS_CBRS_RY_PZT1_CTRL_RSET'] = 2
	    self.timer['clear_hist'] = 1

    def run(self):
        if self.timer['clear_hist']:
            return 'PZT_ON'

class PZT_ON(GuardState):
    index = 20
    def main(self):	
        ezca['NGN-CS_CBRS_RY_PZT2_CTRL_GAIN'] = .25
        ezca['NGN-CS_CBRS_RY_PZT1_CTRL_GAIN'] = .25
        self.timer['pzt_gain'] = ezca['NGN-CS_CBRS_RY_PZT1_CTRL_TRAMP']+5
    def run(self):
        if self.timer['pzt_gain']:
            return 'DAMPING_ON'
    

class DAMPING_ON(GuardState):
    index = 25
    def main(self):
	    ezca['NGN-CS_CBRS_RY_DAMP_GAIN'] = .1
	    self.timer['chill'] = 120

    def run(self):
        if self.timer['chill']:
            return 'RUN'

######################
edges = [
    ('INIT', 'RUN'),
    ('RUN','TURN_OFF_DAMPING'),
    ('TURN_OFF_DAMPING','TURN_OFF_PZT_CTRL'),
    ('TURN_OFF_PZT_CTRL','CLEAR_HISTORY'),
    ('CLEAR_HISTORY','PZT_ON'),
    ('PZT_ON','DAMPING_ON'),
    ('DAMPING_ON','RUN')
    ]
